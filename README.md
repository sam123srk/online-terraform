# Terraform

### What is Terraform?
Terrafor is Infrastructre as code tool created by HashiCorp.

### Why we need Terraform?
- To Reduce the time
- To avoide repeatation in work while creating infra
- To reduce the cost of the project
- To reuse the code to create infra
- To able to manage and provision cloud infrastructure with automation

### Terraform Language
- Terraform configuration file is the complete documentation written in Terraform language which will tell Terraform how the infrastructre will be.
- Terraform configuration language is having extension as .tf or .tf.json

#### Terraform language syntax
```shell
<BLOCK TYPE> "<BLOCK NAME>" "<BLOCK LABEL>" {
  # Block body
  <IDENTIFIER> = <EXPRESSION> # Argument
}
```
#### Block Types
1. Provider 
2. Terraform
3. Resource 
4. Data
5. Variable
6. Output
7. Module

### terraform lifecycle
init - to initialise terraform configuration
plan - to get blueprint of the infra configuration
apply - to implement the infra configuration
destroy - to destroy all the infra resources